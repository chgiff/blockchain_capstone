#~/bin/bash

#install dependencies for minimega
sudo apt-get install debootstrap golang-1.9-go net-tools openvswitch-switch qemu-kvm dnsmasq dhclient


#install dependencies for node js server
sudo apt-get install nodejs


#extract miner_rootfs and minirouter_fs
tar -xzvf miner_rootfs.tar.gz
tar -xzvf minirouter_container_rootfs.tar.gz
