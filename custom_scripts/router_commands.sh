
#initialize vm-1 interface
#router vm-4 interface 0 10.0.0.253/24
#router vm-4 commit


router router1 interface 0 10.0.11.1/24
router router1 interface 1 10.0.17.1/30
router router1 route static 10.0.12.0/24 10.0.17.2
router router1 dhcp 10.0.11.0 range 10.0.11.3 10.0.11.252
router router1 dhcp 10.0.11.0 static 00:00:00:00:00:11 10.0.11.253
router router1 commit

router router2 interface 0 10.0.12.1/24
router router2 interface 1 10.0.17.2/30
router router2 route static 10.0.11.0/24 10.0.17.1
router router2 dhcp 10.0.12.0 range 10.0.12.3 10.0.12.254
router router2 commit

#connect vlans
#router router1 commit
#router router2 commit

