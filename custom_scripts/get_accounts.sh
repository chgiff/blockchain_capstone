#!/bin/bash

MM='sudo DO_NOT_SAVE_HERE/bin/minimega -e'

$MM cc prefix accounts
$MM cc exec geth --exec "eth.coinbase" attach
sleep 60
$MM cc responses accounts raw > /tmp/minimega/files/account_list
sed -i '/^$/d' /tmp/minimega/files/account_list #delete empyty lines
sed -r -i "s/Server: /var account_list = [/" /tmp/minimega/files/account_list #ad var at begining
sed -r -i "s/$/, /" /tmp/minimega/files/account_list #add commas
sed -r -i "$ s/,/];/" /tmp/minimega/files/account_list #add ending brace
$MM cc send account_list
$MM cc exec mv /tmp/miniccc/files/account_list /root/account_list.js

$MM cc background /root/start_transactions
