#!/bin/bash

if [ $# -ne 3 ]
then
	echo "Usage: centralized.sh <Number of branches> <Depth of branch (Max 8)> <Clients per router>"
	exit
fi

if [ $3 -gt 8 ]
then
	echo "Specified depth of branch is too large."
	exit
fi

NUM_BRANCHES=$1
BRANCH_DEPTH=$2
CLIENTS_PER_ROUTER=$3

#location of images
IMAGES="/home/minimega/blockchain_capstone"

#location of local minimega instance
MM="sudo /home/minimega/blockchain_capstone/DO_NOT_SAVE_HERE/bin/minimega"

echo "Number of branches: $NUM_BRANCHES"
echo "Depth  of branches: $BRANCH_DEPTH"
echo "Number of clients per router: $CLIENTS_PER_ROUTER"

#if [ !'$MM -nostdin &' ] ; then
#	echo "Minimega instance started"
#else
#	echo "Minimega instance failed to start"
#	exit
#fi

#exit
MM="/home/minimega/blockchain_capstone/DO_NOT_SAVE_HERE/bin/minimega -e"

echo "Starting web server"
$MM web root DO_NOT_SAVE_HERE/web

echo "Setting filesystem to router"
#setup all routers connected by vlans
$MM vm config filesystem $IMAGES/minirouter_container_rootfs

echo "Configuring backbone router"
BACKBONE_VLANS="vm config net"
for((i=1; i<=$NUM_BRANCHES;i++))
do
	BACKBONE_VLANS="$BACKBONE_VLANS vlan$i"
done

$MM vm config tag type router
$MM $BACKBONE_VLANS
echo "Launching backbone"
$MM vm launch container backbone_router

for((i=1; i<=$NUM_BRANCHES;i++))
do
	#configure the beginning of a branch
	echo "Configuring depth 1 on branch $i:"
	echo -e "\tRouter connected to backbone: router$i-1"

	#vlan$i is connected to backbone, vlan$i-1 is first part of branch
	$MM vm config net vlan$i vlan$i-1
	$MM vm launch container router$i-1
	
	#now we go for depth ;)
	for((j=2;j<BRANCH_DEPTH;j++))
	do
		$MM vm config filesystem $IMAGES/minirouter_container_rootfs
		$MM vm config tag type router
		echo -e "\tConfiguring depth $j on branch $i:"
		echo -e "\t\tRouter$i-$j:" 
		$MM vm config net vlan$i-$(($j-1)) vlan$i-$j vlan$i-$j-1
		$MM vm launch container router$i-$j
		
		#add clients to the router
		$MM vm config filesystem $IMAGES/miner_rootfs
		$MM vm config tag type miner
		$MM vm config net vlan$i-$j-1
		$MM vm launch container $CLIENTS_PER_ROUTER
	done

	#last router on branch has only two interfaces
	echo -e "\tConfiguring depth $BRANCH_DEPTH on branch $i:"
	echo -e "\t\tRouter$i-$BRANCH_DEPTH:" 
	$MM vm config filesystem $IMAGES/minirouter_container_rootfs
	$MM vm config tag type router
	$MM vm config net vlan$i-$(($BRANCH_DEPTH - 1)) vlan$i-$BRANCH_DEPTH-1
	$MM vm launch container router$i-BRANCH_DEPTH
		
	#add clients to the router
	$MM vm config filesystem $IMAGES/miner_rootfs
	$MM vm config tag type miner
	$MM vm config net vlan$i-$BRANCH_DEPTH-1
	$MM vm launch container $CLIENTS_PER_ROUTER
	
done

exit

#configure last router to only be on two vlans
echo "Configuring last router"
$MM vm config net vlan$(($TOTAL_VLANS-2)) vlan$(($TOTAL_VLANS-1))
$MM vm config tag type router	
echo "Launching router$NUM_ROUTERS (last router)"
$MM vm launch container router$NUM_ROUTERS

#launch all routers
$MM vm start all


#enable all routers to send out dhcp to their clients
NETWORK_INTERFACE=2


#behavior for the first and last routers will be different
#first and last will only have two interfaces:
#	their vlan
#	connected router


#for first router in chain, setup bootstrap node
echo "Creating router interfaces for router0"
$MM router router0 interface 0 10.0.0.1/24
$MM router router0 interface 1 10.0.1.1/30
$MM router router0 dhcp 10.0.0.0 static 00:00:00:00:00:11 10.0.0.253
$MM router router0 dhcp 10.0.0.0 range 10.0.0.3 10.0.0.252
for((j=0; j<=$(($NUM_ROUTERS*2));j+=2))
do
	if [ "$j" -gt "$NETWORK_INTERFACE" ]
	then	
		$MM router router0 route static 10.0.$j.0/24 10.0.1.2
	fi	
done
$MM router router0 commit

#for all other routers (except the last), there will be three interfaces 
for((i=1; i<=$(($NUM_ROUTERS - 1));i++))
do
	echo "Creating router interfaces for router$i"
	echo "Interfaces for connecting to other routers: $(($NETWORK_INTERFACE-1)) and $(($NETWORK_INTERFACE+1))"
	echo "Interface  for connecting to vlan         : $NETWORK_INTERFACE"
	#initialize all router interfaces
	$MM router router$i interface 0 10.0.$(($NETWORK_INTERFACE - 1)).2/30
	$MM router router$i interface 1 10.0.$NETWORK_INTERFACE.1/24
	$MM router router$i interface 2 10.0.$(($NETWORK_INTERFACE + 1)).1/30

	#setup static routes to forward traffic to two connected vlans
	
	for((j=0; j<=$(($NUM_ROUTERS*2));j+=2))
	do
		if [ "$j" -lt "$NETWORK_INTERFACE" ]
		then	
			$MM router router$i route static 10.0.$j.0/24 10.0.$(($NETWORK_INTERFACE - 1)).1
		elif [ "$j" -gt "$NETWORK_INTERFACE" ]
		then
			$MM router router$i route static 10.0.$j.0/24 10.0.$(($NETWORK_INTERFACE + 1)).2
		fi
	done

	#send out DHCP for the router's vlan
	$MM router router$i dhcp 10.0.$NETWORK_INTERFACE.0 range 10.0.$NETWORK_INTERFACE.3 10.0.$NETWORK_INTERFACE.254
	$MM router router$i commit

	#move up network interface for next router
	NETWORK_INTERFACE=$(($NETWORK_INTERFACE + 2))
done

#for the last router, only two interfaces
echo "Creating router interfaces for router$NUM_ROUTERS"
$MM router router$NUM_ROUTERS interface 0 10.0.$(($NETWORK_INTERFACE - 1)).2/30
$MM router router$NUM_ROUTERS interface 1 10.0.$NETWORK_INTERFACE.1/24
$MM router router$NUM_ROUTERS dhcp 10.0.$NETWORK_INTERFACE.0 range 10.0.$NETWORK_INTERFACE.3 10.0.$NETWORK_INTERFACE.254
for((j=0; j<=$(($NUM_ROUTERS*2));j+=2))
do
	if [ "$j" -lt "$NETWORK_INTERFACE" ]
	then	
		$MM router router$NUM_ROUTERS route static 10.0.$j.0/24 10.0.$(($NETWORK_INTERFACE - 1)).1
	fi	
done
$MM router router$NUM_ROUTERS commit

#configure mining vms
echo "Setting filesystem to miner"
$MM vm config filesystem $IMAGES/miner_rootfs

#setup bootstrap node
echo "Configuring bootstrap node"
$MM vm config net vlan0,00:00:00:00:00:11
$MM vm config tag type bootstrap
echo "Launching bootstrap node"
$MM vm launch container bootstrap

for((i=0;i<=$(($NUM_ROUTERS*2));i+=2))
do
	echo "Configuring hosts on vlan$i"
	$MM vm config net vlan$i
	$MM vm config tag type miner	
	echo "Launching hosts on vlan$i"
	$MM vm launch container $HOSTS_PER_VLAN
done


#start all mining vms
echo "Starting all vms"
$MM vm start all

exit
