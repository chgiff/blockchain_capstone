#!/bin/bash

ROUTERS=$1
HOSTS_PER_VLAN=$2

#location of all images - ???
IMAGES="/home/minimega/blockchain_capstone"

#location of local minimega instance
MM="/home/minimega/blockchain_capstone/DO_NOT_SAVE_HERE/bin/minimega -e"

echo "Starting web server"
$MM web root DO_NOT_SAVE_HERE/web

#create the "backbone"
echo "Creating backbone router"
$MM vm config filesystem $IMAGES/minirouter_container_rootfs
$MM vm config net branch_1 branch_2 branch_3 branch_4
$MM vm config tag type router
$MM vm launch container backbone_router


echo "Creating branch 1 router"
$MM vm config net branch_1 miner_vlan_1
$MM vm launch container branch_1_router

$MM vm config net branch_2 miner_vlan_2
$MM vm launch container branch_2_router

$MM vm config net branch_3 miner_vlan_3
$MM vm launch container branch_3_router

$MM vm config net branch_4 miner_vlan_4
$MM vm launch container branch_4_router

#Start all routing vms
$MM vm start all


#start all the routing stuff....
#setup number of branches the central backbone needs
$MM router backbone_router dhcp 
$MM router backbone_router route ospf 0 0 #branch 1 in area 0
$MM router backbone_router route ospf 0 1 #branch 2 in area 0
$MM router backbone_router route ospf 0 2 #branch 3
$MM router backbone_router route ospf 0 3 #branch 4

$MM router branch_1_router dhcp 10.0.0.0 static 00:00:00:00:00:11 10.0.0.253

$MM router branch_1_router route ospf 0 0 #branch_1 interface added to area 0
$MM router branch_1_router route ospf 1 1 #miner_vlan_1 added to area 1
$MM router branch_2_router route ospf 0 0 #branch_1 interface added to area 0
$MM router branch_2_router route ospf 1 2 #miner_vlan_2 added to area 2
$MM router branch_3_router route ospf 0 0 #branch_1 interface added to area 0
$MM router branch_3_router route ospf 1 3
$MM router branch_4_router route ospf 0 0 #branch_1 interface added to area 0
$MM router branch_4_router route ospf 1 4 #miner_vlan_4 added to area 4

#add all the miners
$MM vm config filesystem $IMAGES/miner_rootfs
$MM vm config tag type bootstrap
$MM vm config net miner_vlan_1,00:00:00:00:00:11
echo "Launching bootstrap node"
$MM vm launch container bootstrap

$MM vm config tag type miner
$MM vm config net miner_vlan_2
$MM vm launch container 3

$MM vm config net miner_vlan_3
$MM vm launch container 3

$MM vm config net miner_vlan_4
$MM vm launch container 3

