#create web interface
web

#vm config disk minirouter.qcow2
vm config filesystem minirouter_container_rootfs

vm config net vlan1 vlan3
vm launch container router1

vm config net vlan2 vlan3
vm launch container router2


#start mining vms
vm config filesystem miner_rootfs

#bootstrap node
vm config net vlan1,00:00:00:00:00:11
vm launch container bootstrap

#still on vlan 1
vm config net vlan1
vm launch container 5

vm config net vlan2
vm launch container 5

#start all
vm start all

