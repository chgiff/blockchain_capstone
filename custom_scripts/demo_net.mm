#create web interface
web root DO_NOT_SAVE_HERE/web

#vm config disk minirouter.qcow2
vm config filesystem minirouter_container_rootfs
vm config tag type router

#vlan 11 and 12 are ptp links
vm config net vlan1 vlan11
vm launch container router1

vm config net vlan2 vlan11 vlan12
vm launch container router2

vm config net vlan3 vlan12 vlan13
vm launch container router3

vm config net vlan4 vlan13 
vm launch container router4

#start mining vms
vm config filesystem miner_rootfs

#bootstrap node
vm config tag type bootstrap
vm config net vlan1,00:00:00:00:00:11
vm launch container bootstrap

#still on vlan 1
vm config tag type miner
vm config net vlan1
vm launch container 2 #just to have an even number of vms on each vlan

vm config net vlan2
vm launch container 3

vm config net vlan3
vm launch container 3

vm config net vlan4
vm launch container 3

#start all
vm start all

shell sleep 10

#enable all routers to send out dhcp to their clients

router router1 interface 0 10.0.11.1/24
router router1 interface 1 192.168.1.1/30

router router1 route static 10.0.12.0/24 192.168.1.2
router router1 route static 10.0.13.0/24 192.168.1.2
router router1 route static 10.0.14.0/24 192.168.1.2

router router1 dhcp 10.0.11.0 range 10.0.11.3 10.0.11.251
router router1 dhcp 10.0.11.0 static 00:00:00:00:00:11 10.0.11.253
router router1 commit

shell sleep 10

router router2 interface 0 10.0.12.1/24
router router2 interface 1 192.168.1.2/30
router router2 interface 2 192.168.1.5/30

router router2 route static 10.0.11.0/24 192.168.1.1
router router2 route static 10.0.13.0/24 192.168.1.6
router router2 route static 10.0.14.0/24 192.168.1.6

router router2 dhcp 10.0.12.0 range 10.0.12.4 10.0.12.254
router router2 commit

shell sleep 10

router router3 interface 0 10.0.13.1/24
router router3 interface 1 192.168.1.6/30
router router3 interface 2 192.168.1.9/30

router router3 route static 10.0.11.0/24 192.168.1.5
router router3 route static 10.0.12.0/24 192.168.1.5
router router3 route static 10.0.14.0/24 192.168.1.10

router router3 dhcp 10.0.13.0 range 10.0.13.3 10.0.13.254
router router3 commit

shell sleep 10


router router4 interface 0 10.0.14.1/24
router router4 interface 1 192.168.1.10/30

router router4 route static 10.0.11.0/24 192.168.1.9
router router4 route static 10.0.12.0/24 192.168.1.9
router router4 route static 10.0.13.0/24 192.168.1.9

router router4 dhcp 10.0.14.0 range 10.0.14.3 10.0.14.254
router router4 commit

shell sleep 10


